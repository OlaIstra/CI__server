import React from "react";
import ReactDOM from "react-dom";

import "./index.scss";

import { App } from "./pages/App/App";

const app = <App />;

ReactDOM.render(app, document.getElementById("root"));
